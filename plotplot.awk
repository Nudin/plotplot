#!/usr/bin/awk -f

function prompt() {
   printf("\033[0m\033[%iH", lines-3)
   print("Give amplitude, frequency and phase space separated, add a '+' to overplot previous plot ")
   print("Example: 2 1 0")
   }
function coordsys() {
   t=""
   for ( i=1; i<cols; i++) {
      if ( int(i%(3.14/fscale)) == 0) {
         t=t "+"
         printf("\033[%i;%iH\033[2m%s\033[0m", lines/2+1, i, int((i+1)/(3.14/fscale)) " pi")
      }
      else {
         t=t "-"
      }
   }
   printf("\033[%iH\033[2m%s\033[0m", lines/2, t)
   for ( i=1; i<lines-3; i++) {
      printf("\033[%iH|", i)
   }
   printf("\033[0;1H^")
   printf("\033[%i;%iH>", lines/2, cols-1)
}

BEGIN {
   # Try to detect terminal-size
   if(length(ENVIRON["LINES"]) && length(ENVIRON["COLUMNS"])) {
      lines=ENVIRON["LINES"]
      cols=ENVIRON["COLUMNS"]
   }
   else if ( system("tput clear") == 0 ) {
      "tput cols" |& getline cols
      "tput lines" |& getline lines
   }
   else {
      lines=30
      cols=60
   }
   Ascale=lines/16
   fscale=9.6/cols

   print("\033[1J")
   print("\033[0H")
   prompt()
}

/[0-9]+ [0-9]+ [0-9]+ \+?/ {
   # Read input and normalise them so that users ca give nice integers
   A=$1*Ascale
   f=$2*fscale
   p=$3

   shift=int(lines/2)

   # Clear screen
   # Change color
   if ( length(shift) == 0 || $4 != "+") {
     # Change color
     color=(rand()*1000)%7+30
     printf("\033[2J")
   }
   else {
     # Change color
     color=(color-30+1)%7+30
     printf("\033[%i;0H\033[0J", lines-3)
   }
   coordsys()
  
   printf("\033[%im", color)

   # Loop over x-values
   for(x=1; x<cols; x+=1) {
      # Calculate the function and it's normalised derivation
      y  = -cos(f*x+p)*A+shift;
      dy = -sin(f*x+p);
      
      # Depending on the derivation decide which symbol to use to make plot prettier
      if(dy > 0.8) {
        c="/"
      }
      else if(dy > 0.7) {
        c="-"
      }
      else if(dy > -0.7 && y>A) {
        c="."
      }
      else if(dy > -0.7 && y<A) {
        c="`"
      }
      else if(dy > -0.8) {
        c="-"
      }
      else {
        c="\\"
      }
      
      # Add a letter at the calculated position
      if ( y > 0 && y < lines) { 
         printf("\033[%i;%iH%c", y, x+1, c)
      }
   } 
   prompt()
}
